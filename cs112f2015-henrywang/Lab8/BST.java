/**
  *Henry(Haoyu) Wang
  *CMPSC112 Lab8 Part 1 2 3 and 5
  */
public class BST {

    // start of BST class
    private BTreeNode root;
    private int size;

    public BST() {
        root = null;
        size = 0;
    } //BST (constructor)

    public int size() {
        return size;
    } //size

    public boolean isEmpty() {
        return size == 0;
    } //isEmpty


    /*
    ** This is the setup function for insert
    ** It takes a value as input, and calls the recursive insert function starting at the root
    */
    public void insert(int value) {
        // Create new root if it doesn't exist
        if (root == null) {
            root = new BTreeNode(value, null, null, null);
            size = 1;
            return;
        }
        // Insert new value to root if it is exists
        insert(value, root);
        ++size;
    } //add (setup)

    /*
    ** This is the recursive function for insert
    ** It takes a value and a BTreeNode as input, and either adds a node at the current location
    ** (if null), or travels to the left or right child to try to insert there
    */
    private void insert(int value, BTreeNode currentLocation) {
        //Increment size of the node, because we'll insert element in this node
        currentLocation.setSize(currentLocation.getSize() + 1);
        //If current value greater than new value, we'll add it to the left node
        if (currentLocation.getValue() > value) {
            // check whether left node already exists in current node
            if (currentLocation.getLeftChild() != null) {
                // call this method recursively, and as parent node will be left node of current node
                insert(value, currentLocation.getLeftChild());
            } else {
                // create new node and set new node as left child of current node
                BTreeNode newNode = new BTreeNode(value, currentLocation, null, null);
                currentLocation.setLeftChild(newNode);
            }
        } else { // in other case, we'll add it to right node
            // check whether right node already exists in current node
            if (currentLocation.getRightChild() != null) {
                // call this method recursively, and as parent node will be right node of current node
                insert(value, currentLocation.getRightChild());
            } else {
                // create new node and set new node as right child of current node
                BTreeNode newNode = new BTreeNode(value, currentLocation, null, null);
                currentLocation.setRightChild(newNode);
            }
        }
    } //add (recursive)


    /*
    ** This is the setup function for get
    ** It takes a value as input, and calls the recursive get function starting at the root
    **
    ** If the value isn't found, let's return a -1
    */
    public int get(int value) {
        // if there is no root element return -1
        if (root == null) {
            return -1;
        }
        // search element in the root node
        return get(value, root);
    } //get (setup)

    /*
    ** This is the recursive function for get
    ** It takes a value and a BTreeNode as input, and either finds a node at the current location
    ** (if value=currentLocation.getValue()), or travels to the left or right child to try to find it there
    **
    ** If the value isn't found, let's return a -1
    */
    private int get(int value, BTreeNode currentLocation) {
        // if we've found value - return this value
        if (currentLocation.getValue() == value) {
            return value;
        }
        // if current value is greater than searched value - search in the left node
        if (currentLocation.getValue() > value) {
            // if left node exists - search in the left node
            if (currentLocation.getLeftChild() != null) {
                return get(value, currentLocation.getLeftChild());
            }
            // in case it doesn't exist - return -1;
            return -1;
        }
        // if right node exists - search in the right node
        if (currentLocation.getRightChild() != null) {
            return get(value, currentLocation.getRightChild());
        }
        // in case it doesn't exist - return -1;
        return -1;
    } //get (recursive)


    /*
    ** This is the setup function for remove
    ** It takes a value as input, and calls the recursive remove function starting at the root
    */
    public void remove(int value) {
        // if there is no root element - just exit
        if (root == null) {
            return;
        }
        // if there is root element - find value to remove in the root element
        remove(value, root);
    } //remove (setup)

    /*
    ** This is the recursive function for remove
    ** It takes a value and a BTreeNode as input, and either removes the node at the current location
    ** (if value=currentLocation.getValue()), or travels to the left or right child to try to remove from there
    */
    private void remove(int value, BTreeNode currentLocation) {
        // if we've found node to remove - removing it!
        if (currentLocation.getValue() == value) {
            BTreeNode newChild = currentLocation;
            // if there is right child - find smallest value and it will be new value of current node
            if (currentLocation.getRightChild() != null) {
                // find smallest node
                BTreeNode node = findSmallestNode(currentLocation.getRightChild());
                // remove smallest node
                remove(node.getValue());
                // set new value for current node
                newChild.setValue(node.getValue());
            } else {
                // if there is no right node - left node of the removed node will be instead of it
                newChild = currentLocation.getLeftChild();
                if (newChild != null) {
                    newChild.setParent(currentLocation.getParent());
                }
            }
            BTreeNode parent = currentLocation.getParent();
            // if there is no parent -> it's a root node, need to change root
            if (parent != null) {
                if (parent.getLeftChild() == currentLocation) {
                    parent.setLeftChild(newChild);
                } else {
                    parent.setRightChild(newChild);
                }
                // change size of the parents nodes
                do {
                    parent.setSize(parent.getSize() - 1);
                    parent = parent.getParent();
                } while (parent != null);
            } else {
                root = newChild;
            }
            --size;
        } else {
            if (currentLocation.getValue() > value) {
                if (currentLocation.getLeftChild() != null) {
                    remove(value, currentLocation.getLeftChild());
                }
            } else {
                if (currentLocation.getRightChild() != null) {
                    remove(value, currentLocation.getRightChild());
                }
            }
        }
    } //remove (recursive)

    private BTreeNode findSmallestNode(BTreeNode root) {
        BTreeNode currentNode = root;
        while (currentNode.getLeftChild() != null) {
            currentNode = currentNode.getLeftChild();
        }
        return currentNode;
    }

    private class BTreeNode {

        private int value;
        private int size;
        private BTreeNode parent;
        private BTreeNode leftChild;
        private BTreeNode rightChild;

        public BTreeNode(int v, BTreeNode p, BTreeNode l, BTreeNode r) {
            value = v;
            parent = p;
            leftChild = l;
            rightChild = r;
            size = 0;
        } //BTreeNode (constructor)

        public int getValue() {
            return value;
        } //getValue

        public void setValue(int v) {
            value = v;
        } //set Value

        public BTreeNode getParent() {
            return parent;
        } //getParent

        public void setParent(BTreeNode p) {
            parent = p;
        } //setParent

        public BTreeNode getLeftChild() {
            return leftChild;
        } //getLeftChild

        public void setLeftChild(BTreeNode l) {
            leftChild = l;
        } //setLeftChild

        public BTreeNode getRightChild() {
            return rightChild;
        } //getRightChild

        public void setRightChild(BTreeNode r) {
            rightChild = r;
        } //setRightChild

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }
    } //BTreeNode (internal class)

} //BST

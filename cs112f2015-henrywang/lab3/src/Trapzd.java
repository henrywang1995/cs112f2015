/**
 *Henry(Haoyu) Wang
 *CMPSC112 lab3 Trapzd
 * This class defines a specific figure (trapeze)
 * and needed parameters (otherBase, side1, side2)
 * to calculate area and peremeter.
 *
 */
public class Trapzd extends Quad {
	
	private double otherBase, side1, side2;
	// To store other width (base) and to sides (left and right)
	// of trapeze.
	
	 //Initializes needed parameters form trapeze.
	 // For width and height are used setters from superclass.
	
	public Trapzd (double width, double height, double otherBase, double side1, double side2) {
		this.otherBase = otherBase;
		this.side1 = side1;
		this.side2 = side2;
		setWidth(width);
		setHeight(height);
	} // constructor
	
	//Calculates area of trapeze.

	public void calculateArea () {
		setArea (getHeight() * ((getWidth() + otherBase) / 2));
	} // method
	
	// Calculates perimeter of trapeze.
	 
	public void calculatePerimeter () {
		setPerimeter (getWidth() + otherBase + side1 + side2);
	} // method

} // class

/**
 *Henry(Haoyu) Wang
 *CMPSC112 lab3 Shape
 * This is a common class that defines
 * the general features for all figures.
 *  
 */
public abstract class Shape {
	
	private double perimeter, area;
	// To store perimeter and area.
	
	/*
	 * Getter.
	 */
	public double getPerimeter() {
		return perimeter;
	} // method

	public void setPerimeter(double perimeter) {
		this.perimeter = perimeter;
	} // method

	public double getArea() {
		return area;
	} // method

	public void setArea(double area) {
		this.area = area;
	} //  method

} // class

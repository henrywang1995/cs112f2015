/**
 *Henry(Haoyu) Wang
 *CMPSC112 lab3 Quad
 * This class defines to parameters (width and height)
 * which needed for calculating perimeter and area
 * from all figures from subclasses.
 *
 */
public  class Quad extends Shape {

	private double width, height;
	// To store width and height.

	public double getWidth() {
		return width;
	} // method

	public void setWidth(double width) {
		this.width = width;
	} // method

	public double getHeight() {
		return height;
	} // method

	public void setHeight(double height) {
		this.height = height;
	} // method
	
} // class

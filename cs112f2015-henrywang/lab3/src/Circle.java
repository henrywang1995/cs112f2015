/**
 *Henry(Haoyu) Wang
 *CMPSC112 lab3 Circle
 * This class defines a specific figure (circle) and
 * variable (radius)
 * which is used for calculating area and 
 * perimeter this figure.
 *  
 */
public class Circle extends Round {
	
	private double radius;
	// To store radius of circle.
	
	public Circle (double radius) {
		this.radius = radius;
	} // constructor
	
	// Calculates area of circle.

	public void calculateArea () {
		setArea (getPi() * radius * radius);
	} // method
	
	//Calculates perimeter of circle.
	  
	
	public void calculatePerimeter () {
		setPerimeter (2 * getPi() * radius);
	} // method

} // class

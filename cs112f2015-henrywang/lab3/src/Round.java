/**
 *Henry(Haoyu) Wang
 *CMPSC112 lab3 Round
 * This class defines a common variable (pi) 
 * which needed to calculate area and perimeter
 * for all figures from subclasses.
 * 
 */
public abstract class Round extends Shape {

	private final double pi = 3.1415926535f;
	// To store PI.

	public double getPi() {
		return pi;
	} // method

} // class

/**
 *Henry(Haoyu) Wang
 *CMPSC112 lab3 Rect
 * This class defines a common figure (rectangle) 
 * and methods to calculate area and perimeter for
 * figures from subclasses. Here are used a variables
 * (width and height) from superclass Quad.
 *
 */
public  class Rect extends Quad {
	
	// Initializes width and height using
	 //setters from superclass Quad.
	 
	public Rect (double width, double height) {
		setWidth(width);
		setHeight(height);
	} // constructor

	// Calculates area of rectangle.

	public void calculateArea () {
		setArea (getWidth() * getHeight());
	} // method
	
	// Calculates perimeter of rectangle.
	 
	public void calculatePerimeter () {
		setPerimeter ((getHeight() + getWidth()) * 2);
	} // method

} // class

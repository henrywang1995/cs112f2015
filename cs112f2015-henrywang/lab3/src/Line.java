/**
 *Henry(Haoyu) Wang
 *CMPSC112 lab3 Line
 * This class defines two points which
 * make a line.
 *
 */
public class Line extends Shape {
	
	private double x1, x2, y1, y2;
	// To store coordinates of 2 points.
	
	//Initializes all coordinates.

	public Line (double x1, double x2, double y1, double y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	} // constructor
	
	//Calculates the area of line.

	public void calculateArea () {
		setArea (0.0);
	} // method
	
	//Calculates perimeter of line using coordinates of 2 points.
	 
	public void calculatePerimeter () {
		setPerimeter (2 * Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)));
	} // method

} // class

/**
 *Henry(Haoyu) Wang
 *CMPSC112 lab3 Oval
 * This class defines a specific figure (oval) and
 * variables (minor, major)
 * which are used for calculating area and 
 * perimeter this figure.
 *
 */
public class Oval extends Round {

	private double major, minor;
	// To store major and minor
	// for oval.
	
	//Initializes to parameters of oval: major and minor

	public Oval (double major, double minor) {
		this.major = major;
		this.minor = minor;
	} // constructor
	
	// Calculates area of oval.
	 
	public void calculateArea () {
		setArea (getPi() * major * minor);
	} // method
	
	//Calculates perimeter of oval.

	public void calculatePerimeter () {
		setPerimeter (2 * getPi() * Math.sqrt((Math.pow(major, 2) + Math.pow(minor, 2)) / 2));
	} // method
	
} // class

/**
 *Henry(Haoyu) Wang
 *CMPSC112 lab3 Square
 * This class defines a specific figure (Square) as
 * a subclass of Rect.
 *
 */
public class Square extends Rect {

	//to calculate perimeter and area.

	public Square(double width ) {
		 super (width, width);
	} // constructor
	
	//Calculates area of square.

	public void  calculateArea () {
		setArea (getWidth() * getWidth());
	} // method
	
	// Calculates perimeter of square.

	public void  calculatePerimeter () {
		setPerimeter (getWidth() * 4);
	} // method

} // class

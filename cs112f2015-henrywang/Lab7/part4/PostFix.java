/**
  *Henry(Haoyu) Wang
  *CMPSC112 Lab7 PostFix.java part4
  */
import java.util.*;
import java.util.Stack;
public class PostFix {
    
    public static void main(String[] args) {
        for(;;) {
            Scanner scan = new Scanner(System.in);
            String str = scan.next();
            System.out.println("The equation is: "+str);
                    str=convert(str);
            System.out.println("The PostFix equation is: "+str);
            System.out.println("The result is: " 
                    + Integer.toString(calculate(str)));
        }
    }
    public static String convert(String str) {
        String s = " ";
	Stack<Character> stack = new Stack<Character>();
        for (int i = 0; i < str.length(); i++) {
            char chValue = str.charAt(i);
            if (chValue == '(') {
                stack.push('(');
            } else if (chValue == ')') {
                Character oper;
                while (! (oper = stack.peek()).equals('(') 
                        && ! oper.equals(')')) {
                    s += oper.charValue();
                    stack.pop();
                }
                stack.pop();
            } else if (chValue == '+' || chValue == '-') {
                //Stack is empty
                if (stack.isEmpty()) {
                    stack.push(chValue);
                    //current Stack is not empty
                } else {
                    Character oper;
                    while (! (stack.isEmpty() 
                            || (oper = stack.peek()).equals(new Character('(')))) { 
                        s += oper.charValue();
                    	stack.pop();
                    }
                    stack.push(chValue);
                }
            } else if (chValue == '*' || chValue == '/') {
                if (stack.isEmpty()) {
                    stack.push(chValue);
                } else {
                    Character oper;
                    while (! (stack.isEmpty() 
                            || (oper = stack.peek()).equals(new Character('(')) 
                            || oper.equals(new Character('+'))
                            || oper.equals(new Character('-')))) {
                        s += oper.charValue();
                        stack.pop();
                    }
                    stack.push(chValue);
                }
            } else {
                s += chValue;
            }
        }
        while (! stack.isEmpty())
            s += stack.pop().charValue();
        return s;
    }

    public static int calculate(String postfix) {
        Stack<Integer> stack = new Stack<Integer>();
        for (int i = 0; i < postfix.length(); i++) {
            char chValue = postfix.charAt(i);
            if (chValue == '+') 
                stack.push(stack.pop() + stack.pop());
            else if (chValue == '*')
                stack.push(stack.pop()*stack.pop());
            else if (chValue == '-') {
                Integer b = stack.pop();
                stack.push(stack.pop() - b);
            } else if (chValue == '/') {
                Integer b = stack.pop();
                stack.push(stack.pop() / b);
            } else
                stack.push(chValue - '0');
        }
        return stack.pop();
    }
}

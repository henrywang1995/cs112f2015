/**
  *Henry(Haoyu) Wang
  *CMPSC112 Lab7 PostFix.java
  *this program will calculate Infix to Postfix. 
  */
import java.util.*;
import java.util.Stack;
public class PostFix {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str = scan.next();
        System.out.println("The equation is: "+str);
		str=convert(str);
        System.out.println("The PostFix equation is: "+str);
    }
    public static String convert(String str) {
        String s = " ";
		Stack<Character> stack = new Stack<Character>();
        for (int i = 0; i < str.length(); i++) {
            char chValue = str.charAt(i);
            if (chValue == '(') {
                stack.push('(');
            } else if (chValue == ')') {
                Character oper = stack.peek();
                while (!(oper.equals('(')) && !(stack.isEmpty())) {
                    s += oper.charValue();
                    stack.pop();
                    oper = stack.peek();
                }
                stack.pop();
            } else if (chValue == '+' || chValue == '-') {
                //Stack is empty
                if (stack.isEmpty()) {
                    stack.push(chValue);
                    //current Stack is not empty
                } else {
                    Character oper = stack.peek();
                    while (!(stack.isEmpty() || oper.equals(new Character('(')) || oper.equals(new Character(')')))) {
                    	stack.pop();
                        s += oper.charValue();
                    }
                    stack.push(chValue);
                }
            } else if (chValue == '*' || chValue == '/') {
                if (stack.isEmpty()) {
                    stack.push(chValue);
                } else {
                    Character oper = stack.peek();
                    while (!oper.equals(new Character('+')) && !oper.equals(new Character('-')) && !stack.isEmpty()) {
                        stack.pop();
                        s += oper.charValue();
                    }
                    stack.push(chValue);
                }
            } else {
                s += chValue;
            }
        }
        while (! stack.isEmpty()) {
            Character oper = stack.peek();
            if (!oper.equals(new Character('('))) {
                stack.pop();
                s += oper.charValue();
            }
        }
        return s;
    }

}

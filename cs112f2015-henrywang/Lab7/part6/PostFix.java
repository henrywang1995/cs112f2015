/**
  *Henry(Haoyu) Wang
  *CMPSC112 Lab7 PostFix.java part6
  */

import java.util.*;
import java.util.Stack;
public class PostFix {

    public static void main(String[] args) {
        for(;;) {
            Scanner scan = new Scanner(System.in);
            String str = scan.next();
            System.out.println("The equation is: "+str);
                    str=convert(str);
            System.out.println("The PostFix equation is: "+str);
            System.out.println("The result is: " 
                    + Integer.toString(calculate(str)));
        }
    }
    
    public static String convert(String str) {
        String s = " ";
	Stack<Character> stack = new Stack<Character>();
        for (int i = 0; i < str.length(); i++) {
            char chValue = str.charAt(i);
            if (chValue == '(') {
                stack.push('(');
            } else if (chValue == ')') {
                Character oper;
                while (! (oper = stack.peek()).equals('(')) {
                    s += oper.charValue();
                    stack.pop();
                }
                stack.pop();
            } else if (chValue == '+' || chValue == '-') {
                //Stack is empty
                if (stack.isEmpty()) {
                    stack.push(chValue);
                    //current Stack is not empty
                } else {
                    Character oper;
                    while (! (stack.isEmpty() 
                            || (oper = stack.peek()).equals(new Character('(')))) { 
                        s += oper.charValue();
                    	stack.pop();
                    }
                    stack.push(chValue);
                }
            } else if (chValue == '*' || chValue == '/') {
                if (stack.isEmpty()) {
                    stack.push(chValue);
                } else {
                    Character oper;
                    while (! (stack.isEmpty() 
                            || (oper = stack.peek()).equals(new Character('(')) 
                            || oper.equals(new Character('+'))
                            || oper.equals(new Character('-')))) {
                        s += oper.charValue();
                        stack.pop();
                    }
                    stack.push(chValue);
                }
            } else if (chValue == '^') {
                // Power operator: almost an exact copy of the * operator
                // except adding two lower precedence operators, * and /, 
                // to + and -
                if (stack.isEmpty()) {
                    stack.push(chValue);
                } else {
                    Character oper;
                    while (! (stack.isEmpty() 
                            || (oper = stack.peek()).equals(new Character('(')) 
                            || oper.equals(new Character('*'))
                            || oper.equals(new Character('/'))
                            || oper.equals(new Character('+'))
                            || oper.equals(new Character('-')))) {
                        s += oper.charValue();
                        stack.pop();
                    }
                    stack.push(chValue);
                }
            } else if (chValue >= '0' && chValue <= '9') {
                // The beginning of the number:
                // Add '$' and the first digit
                s += '$';
                s += chValue;
                // add all subsequent digits
                for (i++; i < str.length() 
                        && (chValue = str.charAt(i)) >= '0'
                        && chValue <= '9';
                        i++ )
                    s += chValue;
                // End the number with '$'
                s += '$';
                // Set the current position to the last digit, so after 
                // i++ in the outer loop it will point on the next character 
                // in the inpurt string
                i--;
            }
        }
        while (! stack.isEmpty())
            s += stack.pop().charValue();
        return s;
    }

    public static int calculate(String postfix) {

        Stack<Integer> stack = new Stack<Integer>();
        
        for (int i = 0; i < postfix.length(); i++) {
            char chValue = postfix.charAt(i);
            if (chValue == '+') 
                stack.push(stack.pop() + stack.pop());
            else if (chValue == '*')
                stack.push(stack.pop() * stack.pop());
            else if (chValue == '-') {
                Integer b = stack.pop();
                stack.push(stack.pop() - b);
            } else if (chValue == '/') {
                Integer b = stack.pop();
                stack.push(stack.pop() / b);
            } else if (chValue == '^') {
                Integer b = stack.pop();
                Integer a = stack.pop();
                stack.push((int)Math.pow(a, b));
            } else if (chValue == '$') {
                // A number
                int x = 0;
                // Read it completely
                for (i++;
                        i < postfix.length()
                        && (chValue = postfix.charAt(i)) != '$';
                        i++)
                    x = x * 10 + (chValue - '0');
                // After the loop i is set to the closing '$' character, so
                // after i++ in the outer loop i will be set on the next 
                // operation
                stack.push(x);
            } else if (chValue >= '0' && chValue <= '9') 
                // A single digit (won't appear in a postfix generated
                // by this program, but just in case)
                stack.push(chValue - '0');
        }
        return stack.pop();
    }
}

/************************************
* Descripiton: This program will print "Weeee!"
* Compilation: javac Weeee.java
* Excution:    java Weeee   
*************************************/
public class Weeee {                           // The name of this class is Weeray.

    public static void weeee() {
		System.out.println("Weeee!");  // Display "Weeee!" to the standard output.
		weeee();
    } //weeee

    public static void main(String[] args) {   //Display the string.
		weeee();
    } //main

} //Weeee (class)

/****************************************************
* Description: This program will print "Hooray!"
* Compilation: javac Hooray.java
* Execution:   java Hooray
****************************************************/
public class Hooray {                                      //The name of this class is Hooray

    public static void hooray() {                           

		while(true) {                              // This is a while loop will print the statement "Hooray!"
			System.out.println("Hooray!");     
    	} //while

    } //hooray

    public static void main(String[] args) {               // Display the string.
		hooray();
    } //main

} //Hooray (class)

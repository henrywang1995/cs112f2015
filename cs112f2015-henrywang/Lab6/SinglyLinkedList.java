/**
  *Henry(Haoyu) Wang
  *CMPSC112 Lab6 SinglyLinkedList.java
  *
  */

import java.util.LinkedList;

public class SinglyLinkedList {

	public static void main (String [] args) {

		int N = 100000; //10 000, 50 000, 100 000 respectively.

		int[] myArray = new int[N];		
	     long t1 = System.nanoTime();
		for (int i = 0; i < N; i++) {
			myArray[i] = i;
		} //for
		long t2 = System.nanoTime();
		long t3 = t2 - t1;
		System.out.println("Array add: " + t3);
		
		LinkedList<Integer> myList = new LinkedList<Integer>();
		t1 = System.nanoTime();
		for (int i = 0; i < N; i++) {
			myList.add(i);
		} //for
		t2 = System.nanoTime();
	    t3 = t2 - t1;
		System.out.println("LinkedList add: " + t3);
		
		SinglyLinkedList mySList = new SinglyLinkedList();
		t1 = System.nanoTime();
		for (int i = 0; i < N; i++) {
			mySList.add(i);
		} //for
		t2 = System.nanoTime();
	    t3 = t2 - t1;
		System.out.println("SinglyLinkedList add: " + t3);
		
		t1 = System.nanoTime();
		for (int i = 0; i < N; i++) {
			int something = myArray[i];
		} //for
		t2 = System.nanoTime();
	    t3 = t2 - t1;
		System.out.println("Array get: " + t3);
		
		t1 = System.nanoTime();
		for (int i = 0; i < N; i++) {
			int variable = myList.get(i);
		} //for
		t2 = System.nanoTime();
	    t3 = t2 - t1;
		System.out.println("LinkedList get: " + t3);
		
		t1 = System.nanoTime();
		for (int i = 0; i < N; i++) {
			int somethingElse = mySList.get(i);
		} //for
		t2 = System.nanoTime();
	    t3 = t2 - t1;
		System.out.println("SinglyLinkedList get: " + t3);
	} // main

	private Node head; // Head of list.
	private Node tail; // Tail of list.

	/**
	 * To add element to the list.
	 */
	public void add(int element){

		Node node = new Node();
		node.setData(element);

		if(head == null){ // Now only one element in the list.
			head = node;
			tail = node;
		} else {
			tail.setNext(node); // Set the next node.
			tail = node; // Set tail.
		}
	}

	/**
	 * To get element with the specific index.
	 */
	public Integer get (int index) {

		Node tmp = head;
		
		/*
		 * Loop through the all nodes.
		 */
		for (int i = 0; tmp != null; i ++, tmp = tmp.getNext() ) {
			if (i == index) {
				return tmp.getData();
			}
		}
		return null;
	}

	/**
	 * Defines node.
	 */
	class Node {

		private Integer data;
		private Node next;

		/*
		 * To get data.
		 */
		public Integer getData() {
			return data;
		}
		
		/*
		 * To set data.
		 */
		public void setData(Integer data) {
			this.data = data;
		}
		
		/*
		 * To get next node.
		 */
		public Node getNext() {
			return next;
		}
		
		/*
		 * To set next node.
		 */
		public void setNext(Node next) {
			this.next = next;
		}
	} // class
} // class


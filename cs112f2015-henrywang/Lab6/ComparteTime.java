/**
  *Henry(Haoyu) Wang
  *CMPSC112 Lab6 ComparteTime.java
  *
  */


public class ComparteTime {

	public static void main(String[] args) {
		
		int N = 100000; // 10 000, 50 000, 100 000 respectively.
		
		String myString = new String(); // Create string.
		for (int i = 0; i < N; i++) { 
		    myString = myString + "a";
		}  //for

		long t1 = System.nanoTime(); // To fix time before loop.	
		for (int i = 0; i < N; i++) {
			myString = myString + "a";
		} //for
		long t2 = System.nanoTime(); // To fix time after loop.	
		long t3 = t2 - t1; // Calculate difference (running time).
		System.out.println("String: " + t3);

		StringBuilder mySB = new StringBuilder();
		for (int i = 0; i < N; i++)  {
		     mySB.append("a");
		} //for

		t1 = System.nanoTime(); // To fix time before loop.	
		for (int i = 0; i < N; i++) {
			mySB.append("a");
		} //for
		t2 = System.nanoTime(); // To fix time after loop.	
		t3 = t2 - t1; // Calculate difference (running time).
		System.out.println("StringBuilder: " + t3);

	} // main

} // class

/**
 *Henry(Haoyu) Wang
 *CMPSC112 lab2part1
 * The is a Guess my number game let human player a number 
 * between 1 and 100, and let cumputer guess it.
 *
 */
import java.util.Random;
import java.util.Scanner;

public class lab2part1 {

    public static void main(String[] args) {
        Random r = new Random();
        Scanner input = new Scanner(System.in);
        String again;
        do {
            int guessingNumber = r.nextInt(100) + 1;
            System.out.println("Hello user!  I’m thinking of a number between 1 and 100.\nSee if you can guess it!");
            int userInput;
            int tries = 0;
            do {
                System.out.print("Your guess: ");
                userInput = Integer.parseInt(input.nextLine());
                if (userInput != guessingNumber) {
                    System.out.println(getAnswer(userInput, guessingNumber));
                }
                tries++;
            } while (userInput != guessingNumber);
            System.out.println("You got it in " + tries + " tries!  My number was " + guessingNumber + ".");
            System.out.print("Would you like to play again (y/n)? ");
            again = input.nextLine();
        } while (again.equalsIgnoreCase("y"));
    }

    public static String getAnswer(int userInput, int guessingNumber) {
        String answer = "";
        if (userInput > guessingNumber) {
            answer = "Your guess was too high!";
        } else if (userInput < guessingNumber) {
            answer = "Your guess was too low!";
        }
        return answer;
    }

}

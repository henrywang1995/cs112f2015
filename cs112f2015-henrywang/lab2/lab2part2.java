/**
 *Henry(Haoyu) Wang
 *CMPSC112 lab2part2
 * This is a guess my number game let computer guess what
 * the human think between 1 and 100
 *
 */
import java.util.Random;
import java.util.Scanner;

public class lab2part2 {

    public static void main(String[] args) {
        Random r = new Random();
        Scanner input = new Scanner(System.in);
        String again;
        do {
            System.out.println("Hello user!  Please pick a number between 1 and 100.");
            int userNumber = Integer.parseInt(input.nextLine());
            int high = 100;
            int low = 1;
            int computerGuess = (high + low) / 2;
            System.out.println("My first guess is " + computerGuess + ".  How did I do?");
            int tries = 1;
            int userResponse = Integer.parseInt(input.nextLine());
            if (userResponse == -1) {
                low = computerGuess;
            } else {
                high = computerGuess;
            }
            while (computerGuess != userNumber) {
                int previous = computerGuess;
                computerGuess = (high + low) / 2;
                System.out.println(getAnswer(previous, userNumber) + " My next guess is " + computerGuess + ". How did I do?");
                userResponse = Integer.parseInt(input.nextLine());
                if (userResponse == -1) {
                    low = computerGuess;
                } else {
                    high = computerGuess;
                }
                tries++;
            }
            System.out.println("Yay, I got it in " + tries + " tries!  Your number was " + userNumber + ".");
            System.out.print("Would you like to play again (y/n)? ");
            again = input.nextLine();
        } while (again.equalsIgnoreCase("y"));
    }

    public static String getAnswer(int AINumber, int guessingNumber) {
        String answer = "";
        if (AINumber > guessingNumber) {
            answer = "Hmmm, my guess was too high.";
        } else if (AINumber < guessingNumber) {
            answer = "Hmmm, my guess was too low.";
        }
        return answer;
    }
}

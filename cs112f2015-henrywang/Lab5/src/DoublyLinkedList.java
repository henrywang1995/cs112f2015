/**
  *Henry(Haoyu) Wang
  *CMPSC112 Lab5 DoublyLinkedList.java
  *
  */
class DoublyLinkedList {

    private static class Node {

        private int data;
        private Node next;
        private Node previous;

        public Node() {
            data = 0;
            next = null;
        } //Node (constructor)

        public Node(int myData, Node newNext, Node newPrevious) {
            data = myData;
            next = newNext;
            previous = newPrevious;
        } // Node (constructor)

        public int getData() {
            return data;
        } // getValue

        public void setData(int myData) {
            data = myData;
        } // setValue

        public Node getNext() {
            return next;
        } // getNext

        public void setNext(Node newNext) {
            next = newNext;
        } // setNext

        public Node getPrevious() {
            return previous;
        } // getPrevious

        public void setPrevious(Node previous) {
            this.previous = previous;
        } // setPrevious
    } //Node (inner class)

    // ------------------------------
    private Node head;
    private Node tail;
    private int size;

    public DoublyLinkedList() {
        head = null;
        tail = null;
        size = 0;
    } //DoublyLinkedList (constructor)

    public int size() {
        return size;
    } //size

    public boolean isEmpty() {
        return size == 0;
    } //isEmpty

    public void add(int myData) {
        Node newNode = new Node(myData, null, tail);
        if (isEmpty()) {
            head = newNode;
        } else {
            tail.setNext(newNode);
        } //if-else
        tail = newNode;
        size++;
    } //add

    public int get(int index) {
        if (index < 0) {
            System.exit(1);
        } else if (index >= size) {
            System.exit(2);
        } else {
            int i = 0;
            Node temp = head;
            while (i != index) {
                i++;
                temp = temp.getNext();
            } //while			
            return temp.getData();
        } //if-else		
        return -1;
    } //get

    public int getFromEnd(int index) {
        if (index < 0) {
            System.exit(1);
        } else if (index >= size) {
            System.exit(2);
        } else {
            int i = 0;
            Node temp = tail;
            while (i != index) {
                i++;
                temp = temp.getPrevious();
            } //while
            return temp.getData();
        } //if-else
        return -1;
    } //getFromEnd

    public void remove(int index) {
        if (index < 0) {
            System.exit(1);
        } else if (index >= size) {
            System.exit(2);
        } else {
            int i = 0;
            Node temp = head;
            while (i != index) {
                i++;
                temp = temp.getNext();
            } //while

            if (temp == head) {
                head = temp.getNext();
            } else {
                temp.getPrevious().setNext(temp.getNext());
            } //if-else

            if (temp == tail) {
                tail = temp.getPrevious();
            } else {
                temp.getNext().setPrevious(temp.getPrevious());
            } //if-else

            size--;
        } //if-else
    } //remove

    public boolean testPreviousLinks() {
        Node current = tail;
        int count = 1;
        while (current.getPrevious() != null) {
            current = current.getPrevious();
            count++;
        } //while
        return ((current == head) && (count == size));
    } //testPreviousLinks

    // EXTRA CREDIT
    public void printAsCircular(int times) {
        int index = 0;
        Node temp = head;
        while(index < (size * times)) {
            System.out.println(temp.getData());
            if(temp == tail) {
                temp = head;
            } else {
                temp = temp.getNext();
            } // if-else
            index++;
        } // while
    } //printAsCircular
} //DoublyLinkedList (class)


/**
  *Henry(Haoyu) Wang
  *CMPSC112 Lab4 SelectionSort.java
  *
  */

public class SelectionSort {
    // set a array for number 8,3,4,2,1
   public static void main(String args[]) {
 
      int[] array = {8,3,4,2,1};
      System.out.print("Start : " );     
      for(int x = 0; x < array.length; x++) {
         System.out.print( " " + array[x]);     
      }
      System.out.println(" "); 
 
      int first; 
      int tmp; 
      int count=1; 
      for ( int i=array.length-1; i>0; i--,count++ ) {
         first = 0;
         for(int j=1; j<=i; j++) {
            if( array[j] > array[first] )  
               first = j;
         }
         tmp = array[first]; 
         array[first] = array[i];
         array[i] = tmp; 
 
	//print out each line
      System.out.print("Line " + count + ": " );
      for(int x = 0; x < array.length; x++) {
         System.out.print( " " + array[x]);
      }
      System.out.println(" ");
 
 
      } 
   }
}
